﻿
CREATE view [dbo].[vwUtilization] as
select
u.Region_Name,
u.District_Name,
u.Resource_Name,
x.Resource_ID,
convert(nvarchar,u.TimeResolution_Start,111)as ReportDate,
--u.TimeResolution_Finish,
(u.Utilization_AvailableTime/60)/60.00 as TotalAvailability_Hrs,
(u.Utilization_WorkEffort/60)/60.00 as TotalWork_Hrs,
(u.Utilization_NAsDuration/60)/60.00 as TotalNA_Hrs,
(u.Utilization_IdleTime/60)/60.00 as Total_Idle_Hrs,
(u.Utilization_LunchDuration/60)/60.00 as Total_Lunch_Hrs,
(u.Utilization_OvertimeWorkeffort/60)/60.00 as Total_Overtime_Hrs,

case 
when (u.Utilization_WorkEffort+u.Utilization_NAsDuration) = 0 then 0.00
when (u.Utilization_WorkEffort+u.Utilization_NAsDuration) >= (u.Utilization_AvailableTime-u.Utilization_LunchDuration) then 100.00
when (u.Utilization_WorkEffort+u.Utilization_NAsDuration) < (u.Utilization_AvailableTime-u.Utilization_LunchDuration) then (((u.Utilization_WorkEffort/60/60.00)+(u.Utilization_NAsDuration/60/60.00))/(((u.Utilization_AvailableTime/60/60.00)-(u.Utilization_LunchDuration/60/60.00)))*100.00)
else 0
end as 'Total_Utilization%'---pay attention to lunchbreaks now

from
(
        select Resource_ID,timeresolution_start,timeresolution_finish, max(W6InstanceID) as InstanceID
        from dbo.W6RP_RESOURCE_UTILIZATION_RPT ur (nolock)
        group by Resource_ID,timeresolution_start,TimeResolution_Finish

) x 

inner join dbo.W6RP_RESOURCE_UTILIZATION_RPT u (nolock) on x.Resource_ID = u.Resource_ID and x.InstanceID = u.W6InstanceID and x.TimeResolution_Start = u.TimeResolution_Start
where
Region_Name not in ( 'Inactive Techs','Vivint365','SolarUSA','Summer 2014', 'Ram Team')
and u.ResourceType_Name = 'Active;Internal;'
and u.Resource_Name not like 'ZTest%'