﻿
CREATE view [dbo].[vwTravel] as
Select top 100 PERCENT
tr2.W6InstanceID as Instance, 
tr2.Time_Resolution_Start as Date,
tr2.Region_Name,
tr2.District_Name,
tr2.Resource_Name,
tr2.Resource_ID,
tr2.Resource_Type_Name,
(tr2.Travel_Distance)/1609.34 as Traveled_Miles,
(tr2.Travel_Time)/60 as Traveled_Time,
tr2.Assignment_Count as Assignment_Count,
tr2.NA_Count as NA_Count
from 
(
        select Resource_ID,Time_Resolution_Start, max(W6InstanceID) as InstanceID
        from dbo.W6RP_TRAVEL tr (nolock)
        group by Resource_ID,Time_Resolution_Start
) x 
inner join dbo.W6RP_TRAVEL tr2 (nolock) on x.Resource_ID = tr2.Resource_ID and x.InstanceID = tr2.W6InstanceID and x.Time_Resolution_Start = tr2.Time_Resolution_Start
order by tr2.Time_Resolution_Start,Region_Name, District_Name