﻿CREATE TABLE [dbo].[NA_DETAILS_EXEC] (
    [PollingID]       INT      NOT NULL,
    [OutputPollingID] INT      NOT NULL,
    [Ping]            DATETIME NOT NULL,
    CONSTRAINT [PK_NA_DETAILS_EXEC] PRIMARY KEY CLUSTERED ([PollingID] ASC, [OutputPollingID] ASC)
);

