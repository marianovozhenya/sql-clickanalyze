﻿CREATE TABLE [dbo].[W6REPORTS_SHARED_EXECUTIONS] (
    [PollingID]            INT      NOT NULL,
    [W6InstanceID]         INT      NOT NULL,
    [Report]               INT      NOT NULL,
    [NumberOfPermutations] INT      NOT NULL,
    [TimeResolutionStart]  DATETIME NOT NULL,
    [TimeResolutionFinish] DATETIME NOT NULL,
    CONSTRAINT [REPORTS_SHARED_EXECUTIONS_PK] PRIMARY KEY CLUSTERED ([Report] ASC)
);

