﻿CREATE TABLE [dbo].[RPT_New_Report_EXEC] (
    [PollingID]       INT      NOT NULL,
    [OutputPollingID] INT      NOT NULL,
    [Ping]            DATETIME NOT NULL,
    CONSTRAINT [PK_RPT_New_Report_EXEC] PRIMARY KEY CLUSTERED ([PollingID] ASC, [OutputPollingID] ASC)
);

