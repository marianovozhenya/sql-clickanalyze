﻿CREATE TABLE [dbo].[W6RP_RESOURCE_UTILIZATION_RPT] (
    [W6InstanceID]                   INT            NOT NULL,
    [W6EntryID]                      INT            NOT NULL,
    [TimeResolution_Start]           DATETIME       NULL,
    [TimeResolution_Finish]          DATETIME       NULL,
    [Region_Key]                     INT            NULL,
    [Region_Name]                    NVARCHAR (256) NULL,
    [District_Key]                   INT            NULL,
    [District_Name]                  NVARCHAR (256) NULL,
    [Resource_ID]                    NVARCHAR (256) NULL,
    [Resource_Key]                   INT            NULL,
    [Resource_Name]                  NVARCHAR (256) NULL,
    [ResourceType_Name]              NVARCHAR (256) NULL,
    [Utilization_AvailableTime]      INT            NULL,
    [Utilization_WorkEffort]         INT            NULL,
    [Utilization_NAsDuration]        INT            NULL,
    [Utilization_LunchDuration]      INT            NULL,
    [Utilization_IdleTime]           INT            NULL,
    [Utilization_Utilization]        FLOAT (53)     NULL,
    [Utilization_OvertimeWorkeffort] INT            NULL,
    CONSTRAINT [PK_RESOURCE_UTILIZATION_RPT] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IDX_W6RPRESOURCEUTILIZATIONRPT_ResourceID_InstanceID_TimeResolutionStart_others]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION_RPT]([Resource_ID] ASC, [W6InstanceID] ASC, [TimeResolution_Start] ASC)
    INCLUDE([Region_Name], [ResourceType_Name], [Resource_Name]);


GO
CREATE NONCLUSTERED INDEX [IDX_W6RPRESOURCEUTILIZATIONRPT]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION_RPT]([Resource_ID] ASC, [TimeResolution_Start] ASC, [TimeResolution_Finish] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESOURCE_UTILIZATION_RPT1]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION_RPT]([TimeResolution_Start] ASC, [W6InstanceID] ASC, [Region_Key] ASC, [District_Key] ASC, [Resource_Key] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESOURCE_UTILIZATION_RPT8]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION_RPT]([Resource_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESOURCE_UTILIZATION_RPT5]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION_RPT]([District_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_RESOURCE_UTILIZATION_RPT3]
    ON [dbo].[W6RP_RESOURCE_UTILIZATION_RPT]([Region_Key] ASC, [W6InstanceID] ASC);

