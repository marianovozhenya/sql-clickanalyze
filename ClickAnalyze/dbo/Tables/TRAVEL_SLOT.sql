﻿CREATE TABLE [dbo].[TRAVEL_SLOT] (
    [PollingID]       INT NOT NULL,
    [OutputPollingID] INT NOT NULL,
    [LeftBorder]      INT NOT NULL,
    [RightBorder]     INT NOT NULL,
    [Active]          INT NOT NULL,
    CONSTRAINT [PK_TRAVEL_SLOT] PRIMARY KEY CLUSTERED ([PollingID] ASC, [RightBorder] ASC)
);

