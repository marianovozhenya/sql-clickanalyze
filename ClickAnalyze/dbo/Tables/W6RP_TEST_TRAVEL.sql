﻿CREATE TABLE [dbo].[W6RP_TEST_TRAVEL] (
    [W6InstanceID]           INT            NOT NULL,
    [W6EntryID]              INT            NOT NULL,
    [Time_Resolution_Start]  DATETIME       NULL,
    [Time_Resolution_Finish] DATETIME       NULL,
    [Region_Key]             INT            NULL,
    [Region_Name]            NVARCHAR (256) NULL,
    [District_Key]           INT            NULL,
    [District_Name]          NVARCHAR (256) NULL,
    [Resource_ID]            NVARCHAR (256) NULL,
    [Resource_Internal]      INT            NULL,
    [Resource_Key]           INT            NULL,
    [Resource_Name]          NVARCHAR (256) NULL,
    [Resource_Type_Name]     NVARCHAR (256) NULL,
    [Travel_Time]            INT            NULL,
    [Travel_Distance]        INT            NULL,
    [Assignment_Count]       INT            NULL,
    [NA_Count]               INT            NULL,
    CONSTRAINT [PK_TEST_TRAVEL] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_TEST_TRAVEL1]
    ON [dbo].[W6RP_TEST_TRAVEL]([Time_Resolution_Start] ASC, [W6InstanceID] ASC, [Region_Key] ASC, [District_Key] ASC, [Resource_Key] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TEST_TRAVEL9]
    ON [dbo].[W6RP_TEST_TRAVEL]([Resource_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TEST_TRAVEL5]
    ON [dbo].[W6RP_TEST_TRAVEL]([District_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TEST_TRAVEL3]
    ON [dbo].[W6RP_TEST_TRAVEL]([Region_Key] ASC, [W6InstanceID] ASC);

