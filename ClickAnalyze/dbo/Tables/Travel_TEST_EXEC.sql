﻿CREATE TABLE [dbo].[Travel_TEST_EXEC] (
    [PollingID]       INT      NOT NULL,
    [OutputPollingID] INT      NOT NULL,
    [Ping]            DATETIME NOT NULL,
    CONSTRAINT [PK_Travel_TEST_EXEC] PRIMARY KEY CLUSTERED ([PollingID] ASC, [OutputPollingID] ASC)
);

