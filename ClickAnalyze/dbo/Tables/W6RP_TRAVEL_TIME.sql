﻿CREATE TABLE [dbo].[W6RP_TRAVEL_TIME] (
    [W6InstanceID]              INT            NOT NULL,
    [W6EntryID]                 INT            NOT NULL,
    [TimeResolution_Start]      DATETIME       NULL,
    [TimeResolution_Finish]     DATETIME       NULL,
    [Region_Key]                INT            NULL,
    [Region_Name]               NVARCHAR (256) NULL,
    [District_Key]              INT            NULL,
    [District_Name]             NVARCHAR (256) NULL,
    [Resource_ID]               NVARCHAR (256) NULL,
    [Resource_Internal]         INT            NULL,
    [Resource_Key]              INT            NULL,
    [Resource_Name]             NVARCHAR (256) NULL,
    [ResourceType_Name]         NVARCHAR (256) NULL,
    [Travel_Time]               INT            NULL,
    [Travel_Distance]           INT            NULL,
    [Assignments_Count]         INT            NULL,
    [Objects_Properties_Finish] DATETIME       NULL,
    [Objects_Properties_Key]    INT            NULL,
    [Objects_Properties_Start]  DATETIME       NULL,
    [Objects_Properties_Task]   INT            NULL,
    CONSTRAINT [PK_TRAVEL_TIME] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_TRAVEL_TIME1]
    ON [dbo].[W6RP_TRAVEL_TIME]([TimeResolution_Start] ASC, [W6InstanceID] ASC, [Region_Key] ASC, [District_Key] ASC, [Resource_Key] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TRAVEL_TIME9]
    ON [dbo].[W6RP_TRAVEL_TIME]([Resource_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TRAVEL_TIME5]
    ON [dbo].[W6RP_TRAVEL_TIME]([District_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_TRAVEL_TIME3]
    ON [dbo].[W6RP_TRAVEL_TIME]([Region_Key] ASC, [W6InstanceID] ASC);

