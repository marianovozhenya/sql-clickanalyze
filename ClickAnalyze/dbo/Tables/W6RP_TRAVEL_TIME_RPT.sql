﻿CREATE TABLE [dbo].[W6RP_TRAVEL_TIME_RPT] (
    [W6InstanceID]          INT            NOT NULL,
    [W6EntryID]             INT            NOT NULL,
    [TimeResolution_Start]  DATETIME       NULL,
    [TimeResolution_Finish] DATETIME       NULL,
    [Region_Name]           NVARCHAR (256) NULL,
    [District_Name]         NVARCHAR (256) NULL,
    [Resource_ID]           NVARCHAR (256) NULL,
    [Resource_Name]         NVARCHAR (256) NULL,
    [ResourceType_Name]     NVARCHAR (256) NULL,
    [Travel_Time]           INT            NULL,
    [Travel_Distance]       INT            NULL,
    [Assignments_Count]     INT            NULL,
    CONSTRAINT [PK_TRAVEL_TIME_RPT] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_TRAVEL_TIME_RPT1]
    ON [dbo].[W6RP_TRAVEL_TIME_RPT]([TimeResolution_Start] ASC, [W6InstanceID] ASC);

