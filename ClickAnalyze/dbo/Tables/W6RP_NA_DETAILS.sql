﻿CREATE TABLE [dbo].[W6RP_NA_DETAILS] (
    [W6InstanceID]                        INT            NOT NULL,
    [W6EntryID]                           INT            NOT NULL,
    [Context_Plan]                        INT            NULL,
    [Context_Plan_Name]                   NVARCHAR (256) NULL,
    [Context_Scenario]                    INT            NULL,
    [Context_Scenario_Name]               NVARCHAR (256) NULL,
    [Time_Resolution_Start]               DATETIME       NULL,
    [Time_Resolution_Finish]              DATETIME       NULL,
    [Path_Region]                         INT            NULL,
    [Path_Region_Name]                    NVARCHAR (256) NULL,
    [Path_District]                       INT            NULL,
    [Path_District_Name]                  NVARCHAR (256) NULL,
    [Resource_Contractor]                 INT            NULL,
    [Resource_Crew]                       INT            NULL,
    [Resource_ID]                         NVARCHAR (256) NULL,
    [Resource_Internal]                   INT            NULL,
    [Resource_Key]                        INT            NULL,
    [Resource_Name]                       NVARCHAR (256) NULL,
    [NA_Details_Comment]                  NVARCHAR (256) NULL,
    [NA_Details_Finish]                   DATETIME       NULL,
    [NA_Details_Key]                      INT            NULL,
    [NA_Details_Location]                 NVARCHAR (256) NULL,
    [NA_Details_NonAvailabilityType]      INT            NULL,
    [NA_Details_NonAvailabilityType_Name] NVARCHAR (256) NULL,
    [NA_Details_Start]                    DATETIME       NULL,
    [NA_Details_Duration]                 INT            NULL,
    CONSTRAINT [PK_NA_DETAILS] PRIMARY KEY CLUSTERED ([W6InstanceID] ASC, [W6EntryID] ASC)
);


GO
CREATE NONCLUSTERED INDEX [IX_NA_DETAILS5]
    ON [dbo].[W6RP_NA_DETAILS]([Time_Resolution_Start] ASC, [W6InstanceID] ASC, [Path_Region] ASC, [Path_District] ASC, [Resource_Key] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_NA_DETAILS15]
    ON [dbo].[W6RP_NA_DETAILS]([Resource_Key] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_NA_DETAILS9]
    ON [dbo].[W6RP_NA_DETAILS]([Path_District] ASC, [W6InstanceID] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_NA_DETAILS7]
    ON [dbo].[W6RP_NA_DETAILS]([Path_Region] ASC, [W6InstanceID] ASC);

