﻿CREATE TABLE [dbo].[New_Travel_EXEC] (
    [PollingID]       INT      NOT NULL,
    [OutputPollingID] INT      NOT NULL,
    [Ping]            DATETIME NOT NULL,
    CONSTRAINT [PK_New_Travel_EXEC] PRIMARY KEY CLUSTERED ([PollingID] ASC, [OutputPollingID] ASC)
);

