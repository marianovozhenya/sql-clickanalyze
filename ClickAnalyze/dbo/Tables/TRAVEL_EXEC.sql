﻿CREATE TABLE [dbo].[TRAVEL_EXEC] (
    [PollingID]       INT      NOT NULL,
    [OutputPollingID] INT      NOT NULL,
    [Ping]            DATETIME NOT NULL,
    CONSTRAINT [PK_TRAVEL_EXEC] PRIMARY KEY CLUSTERED ([PollingID] ASC, [OutputPollingID] ASC)
);

