﻿CREATE TABLE [dbo].[New_Travel_SLOT] (
    [PollingID]       INT NOT NULL,
    [OutputPollingID] INT NOT NULL,
    [LeftBorder]      INT NOT NULL,
    [RightBorder]     INT NOT NULL,
    [Active]          INT NOT NULL,
    CONSTRAINT [PK_New_Travel_SLOT] PRIMARY KEY CLUSTERED ([PollingID] ASC, [RightBorder] ASC)
);

